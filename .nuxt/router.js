import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _43302740 = () => interopDefault(import('..\\pages\\about.vue' /* webpackChunkName: "pages_about" */))
const _2292ec9e = () => interopDefault(import('..\\pages\\contactUs.vue' /* webpackChunkName: "pages_contactUs" */))
const _640f92e5 = () => interopDefault(import('..\\pages\\gallery.vue' /* webpackChunkName: "pages_gallery" */))
const _0fb50762 = () => interopDefault(import('..\\pages\\myvideo.vue' /* webpackChunkName: "pages_myvideo" */))
const _09d0265f = () => interopDefault(import('..\\pages\\play-video.vue' /* webpackChunkName: "pages_play-video" */))
const _3dc7341b = () => interopDefault(import('..\\pages\\privacy.vue' /* webpackChunkName: "pages_privacy" */))
const _227abc88 = () => interopDefault(import('..\\pages\\profile.vue' /* webpackChunkName: "pages_profile" */))
const _0c7d2f7a = () => interopDefault(import('..\\pages\\restaurant.vue' /* webpackChunkName: "pages_restaurant" */))
const _7197f6e5 = () => interopDefault(import('..\\pages\\search.vue' /* webpackChunkName: "pages_search" */))
const _69128f77 = () => interopDefault(import('..\\pages\\template.vue' /* webpackChunkName: "pages_template" */))
const _6d67bda6 = () => interopDefault(import('..\\pages\\uploadvideo.vue' /* webpackChunkName: "pages_uploadvideo" */))
const _189f6a2c = () => interopDefault(import('..\\pages\\auth\\afterregister.vue' /* webpackChunkName: "pages_auth_afterregister" */))
const _e4a3e2cc = () => interopDefault(import('..\\pages\\auth\\confirmation.vue' /* webpackChunkName: "pages_auth_confirmation" */))
const _2b091fd4 = () => interopDefault(import('..\\pages\\auth\\login.vue' /* webpackChunkName: "pages_auth_login" */))
const _7be017db = () => interopDefault(import('..\\pages\\auth\\loginImpress.vue' /* webpackChunkName: "pages_auth_loginImpress" */))
const _f9a839a2 = () => interopDefault(import('..\\pages\\auth\\logout.vue' /* webpackChunkName: "pages_auth_logout" */))
const _ef255d58 = () => interopDefault(import('..\\pages\\auth\\profile.vue' /* webpackChunkName: "pages_auth_profile" */))
const _4dc00ef0 = () => interopDefault(import('..\\pages\\auth\\register.vue' /* webpackChunkName: "pages_auth_register" */))
const _48861352 = () => interopDefault(import('..\\pages\\manage-style\\list-video.vue' /* webpackChunkName: "pages_manage-style_list-video" */))
const _7c5d8972 = () => interopDefault(import('..\\pages\\manage-style\\upload.vue' /* webpackChunkName: "pages_manage-style_upload" */))
const _f4d6950c = () => interopDefault(import('..\\pages\\setups\\setup.vue' /* webpackChunkName: "pages_setups_setup" */))
const _edc1d0fc = () => interopDefault(import('..\\pages\\treecomments\\treecomment.vue' /* webpackChunkName: "pages_treecomments_treecomment" */))
const _811e4634 = () => interopDefault(import('..\\pages\\users\\user.vue' /* webpackChunkName: "pages_users_user" */))
const _4041f6ca = () => interopDefault(import('..\\pages\\editvideo\\_videoid.vue' /* webpackChunkName: "pages_editvideo__videoid" */))
const _3938c865 = () => interopDefault(import('..\\pages\\video\\_videoid.vue' /* webpackChunkName: "pages_video__videoid" */))
const _03c071b6 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about",
    component: _43302740,
    name: "about"
  }, {
    path: "/contactUs",
    component: _2292ec9e,
    name: "contactUs"
  }, {
    path: "/gallery",
    component: _640f92e5,
    name: "gallery"
  }, {
    path: "/myvideo",
    component: _0fb50762,
    name: "myvideo"
  }, {
    path: "/play-video",
    component: _09d0265f,
    name: "play-video"
  }, {
    path: "/privacy",
    component: _3dc7341b,
    name: "privacy"
  }, {
    path: "/profile",
    component: _227abc88,
    name: "profile"
  }, {
    path: "/restaurant",
    component: _0c7d2f7a,
    name: "restaurant"
  }, {
    path: "/search",
    component: _7197f6e5,
    name: "search"
  }, {
    path: "/template",
    component: _69128f77,
    name: "template"
  }, {
    path: "/uploadvideo",
    component: _6d67bda6,
    name: "uploadvideo"
  }, {
    path: "/auth/afterregister",
    component: _189f6a2c,
    name: "auth-afterregister"
  }, {
    path: "/auth/confirmation",
    component: _e4a3e2cc,
    name: "auth-confirmation"
  }, {
    path: "/auth/login",
    component: _2b091fd4,
    name: "auth-login"
  }, {
    path: "/auth/loginImpress",
    component: _7be017db,
    name: "auth-loginImpress"
  }, {
    path: "/auth/logout",
    component: _f9a839a2,
    name: "auth-logout"
  }, {
    path: "/auth/profile",
    component: _ef255d58,
    name: "auth-profile"
  }, {
    path: "/auth/register",
    component: _4dc00ef0,
    name: "auth-register"
  }, {
    path: "/manage-style/list-video",
    component: _48861352,
    name: "manage-style-list-video"
  }, {
    path: "/manage-style/upload",
    component: _7c5d8972,
    name: "manage-style-upload"
  }, {
    path: "/setups/setup",
    component: _f4d6950c,
    name: "setups-setup"
  }, {
    path: "/treecomments/treecomment",
    component: _edc1d0fc,
    name: "treecomments-treecomment"
  }, {
    path: "/users/user",
    component: _811e4634,
    name: "users-user"
  }, {
    path: "/editvideo/:videoid?",
    component: _4041f6ca,
    name: "editvideo-videoid"
  }, {
    path: "/video/:videoid?",
    component: _3938c865,
    name: "video-videoid"
  }, {
    path: "/",
    component: _03c071b6,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}

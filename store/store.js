export const state = () => ({
    active: '1'
})

export const mutations = {
    gallery(state) {
        state.active = '2';
    },
    upload(state) {
        state.active = '3-1';
    },
    myvideo(state) {
        state.active = '3-2';
    },
    styleupload(state) {
        state.active = '4-1';
    },
    stylelistvideo(state) {
        state.active = '4-2';
    },
    setup(state) {
        state.active = '5';
    }
}